# Tests for QMake SCM

This repository contains tests for [QMake SCM](https://gitlab.com/dm0/qmake-scm) project.

Use `tox` to run test and pass path to QMake SCM project root:

```bash
tox -- --qscm-path /path/to/qmake-scm
```

This repository also include docker image that can be used to run tests.