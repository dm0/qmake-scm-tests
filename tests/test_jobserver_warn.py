"""
Test jobserver unavailable warning.
"""
# import sys
import pytest
import runhelpers as run


@pytest.mark.skipif('sys.platform == "win32"')
def test_jobserver(tmpdir, baserepo, datadir, script_runner):
    """
    Test that there are no jobserver warnings.

    Parameters
    ----------
    tmpdir : pathlib.Path
        Temporary directory.
    baserepo : pathlib.Path
        Path to the base repository
    datadir : pathlib.Path
        Path to the data directory
    script_runner : ScriptRunner
        Runner for an executable (fixture).
    """

    repo_path = tmpdir / 'repo'
    repo_path.mkdir()

    build_path = tmpdir / 'build'
    build_path.mkdir()

    # Run test case
    cmdlines, outs = run.run_commands(
        ['git clone {origin} {repo}'],
        {
            'repo': repo_path,
            'data': datadir,
            'origin': baserepo
        },
        cwd=repo_path
    )

    ret = script_runner.run('qmake', repo_path / 'verapp', cwd=build_path)
    assert ret.success

    ret = script_runner.run('make', '-j3', cwd=build_path)
    assert ret.success
    assert 'warning: jobserver unavailable' not in ret.stderr
