QT -= gui qt core
CONFIG += c++11 console
CONFIG -= debug_and_release debug_and_release_target

SOURCES += \
        app.cpp

include(../git.pri)

QSCM_HEADERS += $$PWD/version.in