#include <iostream>
#include "version.h"

int main()
{
    std::cout << "VERSION:" << VERSION << std::endl;
    std::cout << "QSCM_SEMVER:" << QSCM_SEMVER << std::endl;
    std::cout << "QSCM_SEMVER_SIMPLE:" << QSCM_SEMVER_SIMPLE << std::endl;
    std::cout << "QSCM_SEMVER_MAJ:" << QSCM_SEMVER_MAJ << std::endl;
    std::cout << "QSCM_SEMVER_MIN:" << QSCM_SEMVER_MIN << std::endl;
    std::cout << "QSCM_SEMVER_PAT:" << QSCM_SEMVER_PAT << std::endl;
#ifdef QSCM_SEMVER_PREREL
    std::cout << "QSCM_SEMVER_PREREL:" << QSCM_SEMVER_PREREL << std::endl;
#endif
#ifdef QSCM_SEMVER_BUILD
    std::cout << "QSCM_SEMVER_BUILD:" << QSCM_SEMVER_BUILD << std::endl;
#endif
    std::cout << "QSCM_SEMVER_SUFFIX:" << QSCM_SEMVER_SUFFIX << std::endl;
    std::cout << "QSCM_HASH:" << QSCM_HASH << std::endl;
    std::cout << "QSCM_DISTANCE:" << QSCM_DISTANCE << std::endl;
    std::cout << "QSCM_BRANCH:" << QSCM_BRANCH << std::endl;
    std::cout << "QSCM_PRETTY_VERSION:" << QSCM_PRETTY_VERSION << std::endl;

    return 0;
}

