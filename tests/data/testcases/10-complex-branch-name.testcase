# See baserepo.commands for the setup of the origin repository.
# The following substitution will be replaced with corresponding paths:
# - {repo} - Path to repository to operate on
# - {data} - Path to data directory
# - {origin} - Path to origin (local bare repo)

feature: >
  Complex branch names containing "/" are handled correctly.

scenario:
  - git clone {origin} {repo}
  - git config user.email "tester@test.org"
  - git config user.name "Tester"
  - git checkout v0.0.1
  - git checkout -b new/branch/name
  - "!echo '' >> new_branch_name.txt"
  - git add -A
  - git commit -m "Added new_branch_name.txt"
  - git tag v1.0.1

expected:
  VERSION: 1.0.1
  QSCM_SEMVER: 1.0.1
  QSCM_SEMVER_SIMPLE: 1.0.1
  QSCM_SEMVER_MAJ: 1
  QSCM_SEMVER_MIN: 0
  QSCM_SEMVER_PAT: 1
  QSCM_SEMVER_PREREL:
  QSCM_SEMVER_BUILD:
  QSCM_SEMVER_SUFFIX:
  QSCM_HASH:
    re.match: ^[0-9a-f]+$
    message: Hash must be valid hex string and have no dirty indicator
  QSCM_DISTANCE:
    ==: 0
    message: Tag distance must above zero
  QSCM_BRANCH: new/branch/name
  QSCM_PRETTY_VERSION:
    ==format: v{VERSION} {QSCM_HASH} (@{QSCM_BRANCH})
    message: Pretty version must be composition of version, hash and branch name
