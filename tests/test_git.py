"""
Test jobserver unavailable warning.
"""
import sys
import runhelpers as run


def test_git_fatal_messages(tmpdir, baserepo, datadir, script_runner):
    """
    Test that there are no fatal messages printed  by git.

    Parameters
    ----------
    tmpdir : pathlib.Path
        Temporary directory.
    baserepo : pathlib.Path
        Path to the base repository
    datadir : pathlib.Path
        Path to the data directory
    script_runner : ScriptRunner
        Runner for an executable (fixture).
    """

    repo_path = tmpdir / 'repo'
    repo_path.mkdir()

    build_path = tmpdir / 'build'
    build_path.mkdir()

    # Run test case
    cmdlines, outs = run.run_commands(
        ['git clone {origin} {repo}', 'git checkout v0.0.1~1'],
        {
            'repo': repo_path,
            'data': datadir,
            'origin': baserepo
        },
        cwd=repo_path
    )

    ret = script_runner.run('qmake', repo_path / 'verapp', cwd=build_path)
    assert ret.success
    assert 'fatal: No tags can describe' not in (ret.stderr + ret.stdout)


def test_git_not_available_warning(tmpdir, baserepo, datadir, script_runner):
    """
    Test that a warning is printed if git is not available.

    Parameters
    ----------
    tmpdir : pathlib.Path
        Temporary directory.
    baserepo : pathlib.Path
        Path to the base repository
    datadir : pathlib.Path
        Path to the data directory
    script_runner : ScriptRunner
        Runner for an executable (fixture).
    """

    repo_path = tmpdir / 'repo'
    repo_path.mkdir()

    build_path = tmpdir / 'build'
    build_path.mkdir()

    # Run test case
    cmdlines, outs = run.run_commands(
        ['git clone {origin} {repo}', 'git checkout v0.0.1~1'],
        {
            'repo': repo_path,
            'data': datadir,
            'origin': baserepo
        },
        cwd=repo_path
    )

    ret = script_runner.run(
        'qmake', 'QSCM_GIT=not-a-git', repo_path / 'verapp', cwd=build_path)
    assert ret.success
    assert 'Command \'git status\' returned error.' in ret.stderr


def test_custom_git(tmpdir, baserepo, datadir, script_runner):
    """
    Test that there are no fatal messages printed  by git.

    Parameters
    ----------
    tmpdir : pathlib.Path
        Temporary directory.
    baserepo : pathlib.Path
        Path to the base repository
    datadir : pathlib.Path
        Path to the data directory
    script_runner : ScriptRunner
        Runner for an executable (fixture).
    """

    repo_path = tmpdir / 'repo'
    repo_path.mkdir()

    build_path = tmpdir / 'build'
    build_path.mkdir()

    # Run test case
    cmdlines, outs = run.run_commands(
        ['git clone {origin} {repo}', 'git checkout v0.0.1~1'],
        {
            'repo': repo_path,
            'data': datadir,
            'origin': baserepo
        },
        cwd=repo_path
    )

    git = 'git.exe' if sys.platform == 'win32' else '/usr/bin/git'
    ret = script_runner.run(
        'qmake', f'QSCM_GIT={git}', repo_path / 'verapp', cwd=build_path)
    assert ret.success
    assert 'Command \'git status\' returned error.' not in ret.stderr
