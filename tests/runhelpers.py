"""
Helper functions to run different commands and clonning and build shortcuts.
"""
import sys
import subprocess
import shlex
import re


def run_commands(commands, substitutions, cwd=None, timeout=None):
    """
    Run commands from the file.

    Each line in the file executed as a separate command.
    Leading and trailing spaces are srtipped.
    Empty lines and lines starting from `#` are ignored.

    Parameters
    ----------
    commands : Iterable[str]
        Iterable with commands to execute.
    substitutions : dict
        Dictionary with substitutions to perform in the command line.
    cwd : str | Path, optional
        Current workind directory of the commands.
    timeout : Numeric, optional
        Timeout of execution of each command.

    Raises
    ------
    RuntimeError
        Raised if executing command returns non-zero code

    Returns
    -------
    tuple
        Returns (commands, outputs) tuple with list of executed commands (with
        substitutions performed) and their outputs (stdout and stderr merged).

    """
    cmdlines = []
    outputs = []
    for line in commands:
        line = line.strip()
        if line.startswith('#') or line == '':
            continue
        # Check platform requirements
        if line.startswith('['):
            match = re.match(r'^\[([^\]]+)\](.+)', line, re.DOTALL)
            # Skip invalid lines
            if match is None:
                continue
            platform_req = match.group(1)
            line = match.group(2)
            if platform_req.startswith('!'):
                if sys.platform == platform_req[1:]:
                    continue
            elif sys.platform != platform_req:
                continue
        cmdline = line.format(**substitutions)
        cmdlines.append(cmdline)
        run_in_shell = cmdline.startswith('!')
        # Strip "!" symbol (run-in-shell mode)
        if run_in_shell:
            cmdline = cmdline[1:]
        # in normal mode split command line, but not on windows
        elif sys.platform != 'win32':
            cmdline = shlex.split(cmdline)

        # On windows additionally prepend powershell -command
        if sys.platform == 'win32':
            cmdline = 'powershell -command ' + cmdline

        result = subprocess.run(
            cmdline,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            cwd=cwd,
            timeout=timeout,
            universal_newlines=True,
            shell=run_in_shell
        )
        if result.returncode != 0:
            raise RuntimeError(
                f'Command "{cmdline}" failed with exit code '
                f'{result.returncode}.\nCommand output:\n{result.stdout}'
            )
        outputs.append(result.stdout)
    return cmdlines, outputs


def clone_repo(origin, target):
    """
    Clone repository given by origin to directory given by target.

    Parameters
    ----------
    origin : str | Path
        Path to repository to clone.
    target : str | Path
        Path to target directory.
    """
    run_commands(
        ['git clone {origin} {repo}'],
        {
            'repo': target,
            'origin': origin
        }
    )


def build_and_run(project, build_dir, qmake_args='', run='./app',
                  verbose=False):
    """
    Build and then execute Qmake-based application.

    Intended to work with verapp application, that prints version constants
    defined by QMake SCM.
    Build application by calling qmake followed by make and then execute it.
    Captures and parses application output into dict.

    Parameters
    ----------
    project : str | Path
        Path to project containing .pro file.
    build_dir : str | Path
        Path to build directory.
    qmake_args : str, optional
        Additional arguments to add to qmake invocation.
    run : str, optional
        Application to execute after build.
    verbose : bool, optional
        Print executed commands and their output.

    Returns
    -------
    dict
        Dictionary of parsed application output.
    """
    make = 'nmake' if sys.platform == 'win32' else 'make'
    cmdlines, outs = run_commands(
        ['qmake {project} CONFIG+=qscm_debug ' + qmake_args, make, run],
        {'project': project},
        cwd=build_dir
    )
    if verbose:
        for cmdline, out in zip(cmdlines, outs):
            print(cmdline)
            print(out)
    values = {}
    for line in outs[-1].splitlines():
        key, value = line.split(':', 1)
        values[key] = value
    return values
