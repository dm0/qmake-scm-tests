"""
Test execution of C++ console app.
"""
import re
import yaml

import runhelpers as run


TEST_OPS = {
    '>': lambda x, y, v: type(y)(x) > y,
    '<': lambda x, y, v: type(y)(x) < y,
    '>=': lambda x, y, v: type(y)(x) >= y,
    '<=': lambda x, y, v: type(y)(x) <= y,
    '==': lambda x, y, v: type(y)(x) == y,
    '!=': lambda x, y, v: type(y)(x) != y,
    're.match': lambda x, y, v: re.match(y, x) is not None,
    '!re.match': lambda x, y, v: re.match(y, x) is None,
    '==format': lambda x, y, v: x == y.format(**v)
}


def test_cases(testcase, tmpdir, baserepo, datadir, request):
    """
    Test that application requires parameters.

    It also expected to print error message containing "Not enough arguments".

    Parameters
    ----------
    script_runner : ScriptRunner
        Runner for an executable (fixture).
    """

    repo_path = tmpdir / 'repo'
    repo_path.mkdir()

    build_path = tmpdir / 'build'
    build_path.mkdir()

    verbose = request.config.getoption("--verbose") > 0

    # Load case
    with open(testcase, 'r') as fp:
        case = yaml.load(fp, Loader=yaml.FullLoader)

    request.node.add_report_section(
        'setup',
        'feature',
        'Feature [{0}]:\n{1}'.format(testcase.name, case['feature'])
    )

    # Run test case
    cmdlines, outs = run.run_commands(
        case['scenario'],
        {
            'repo': repo_path,
            'data': datadir,
            'origin': baserepo
        },
        cwd=repo_path
    )
    if verbose:
        for cmdline, out in zip(cmdlines, outs):
            print(cmdline)
            print(out)

    out = run.build_and_run(
        repo_path / 'verapp', build_path,
        qmake_args=case.get('qmake_args', ''), verbose=verbose
    )
    if verbose:
        print('Application output:', out)

    case_response = case['expected']

    for key, value in case_response.items():
        assert key in out, \
            f'Response key {key} not found in application output {out}'
        if isinstance(value, str):
            assert value == out[key], f'{key} must be {value}'
        elif isinstance(value, dict):
            for opname, oparg in value.items():
                if opname == 'message':
                    continue
                assert opname in TEST_OPS, \
                    f'Unknown operation {opname} in testcase response file'
                message = (f'Condition "{opname}" failed for {key} with value '
                           f'"{out[key]}" and argument "{oparg}"')
                if 'message' in value:
                    message += ':\n' + value['message']
                assert TEST_OPS[opname](out[key], oparg, out), message
