"""
Fixtures and helper functions to use in tests.
"""
import pytest

from pathlib import Path

from runhelpers import run_commands


def pytest_addoption(parser):
    """
    Add command line option --qscm-path.

    Parameters
    ----------
    parser : argparse.Parser
        Argument parser object.
    """
    parser.addoption(
        '--qscm-path', required=True, help='Path to QMake SCM project root'
    )


def pytest_generate_tests(metafunc):
    """
    Parametrize tests with testcases from corresponding data directory.

    Hooks into pytest process and parametrize test functions accepting
    `testcase` fixture (not a real fixture).

    Parameters
    ----------
    metafunc : pytest.Metafunc
        Pytest machinery that allows parametrization.
    """
    if 'testcase' in metafunc.fixturenames:
        cases_path = Path(__file__).resolve().parent / 'data/testcases'
        cases = []
        names = []
        for case in cases_path.glob('*.testcase'):
            cases.append(case)
            names.append(case.stem)
        metafunc.parametrize('testcase', cases, ids=names)


@pytest.fixture(scope='session')
def qscmpath(request):
    """
    Return QMake SCM project path pased with pytest invocation.

    Parameters
    ----------
    request : Request
        PyTest request.

    Returns
    -------
    Path
        Path to QMake SCM project root as passed via command line.
    """
    return Path(request.config.getoption('--qscm-path')).resolve()


@pytest.fixture(scope='session')
def datadir():
    """
    Session-level fixture returning path to tests data directory.

    Returns
    -------
    Path
        Path to tests data directory.
    """
    basedir = Path(__file__).resolve().parent
    return basedir / 'data'


@pytest.fixture(scope='session')
def baserepo(tmp_path_factory, datadir, qscmpath):
    """
    Session-level fixture that creates bare base repository and returns path.

    Base repository has simaple command line application that can be built
    using qmake followed by make command. When executed, command line
    application prints values of defines generated with QMake SCM prefixed with
    names of the defines.

    Parameters
    ----------
    tmp_path_factory : fixture
        Built-in fixture, returns factory for creating temporary directories.
    datadir : fixture
        Test data directory fixture.

    Returns
    -------
    Path
        Path to base repository suitable for clonning (bare repository).
    """
    bare_repo_path = tmp_path_factory.mktemp('bare')
    working_repo_path = tmp_path_factory.mktemp('wcopy')
    with open(datadir / 'baserepo.commands', 'r') as fp:
        run_commands(
            fp,
            {
                'repo': working_repo_path,
                'data': datadir,
                'origin': bare_repo_path,
                'qscm': qscmpath
            },
            cwd=working_repo_path
        )
    return bare_repo_path
